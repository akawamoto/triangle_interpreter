import json


def lambda_handler(event, context):
    side_dict = event['queryStringParameters'] or {}
    side_args = (side_dict.get(side) for side in ('a', 'b', 'c'))

    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps({'message': interpret_triangle(*side_args)})
    }


def interpret_triangle(*side_args):
    """Given three side lengths, return the type of triangle"""

    try:
        sides = list(map(int, side_args))

    except (ValueError, TypeError):
        return 'Error'

    if not all(map((lambda x: x > 0), sides)):
        return 'Error'

    number_of_different_lengths = len(set(sides))

    if number_of_different_lengths == 1:
        return 'Equilateral'

    elif number_of_different_lengths == 2:
        return 'Isosceles'

    return 'Scalene'
