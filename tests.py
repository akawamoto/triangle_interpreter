import unittest

from triangle_interpreter import interpret_triangle


class TestTriangleInterpreter(unittest.TestCase):

    def test_invalid_args(self):
        self.assertEqual(interpret_triangle(None, None, None), 'Error')
        self.assertEqual(interpret_triangle(1, 2, 'a'), 'Error')
        self.assertEqual(interpret_triangle(1, 2, None), 'Error')

    def test_interpreter(self):
        self.assertEqual(interpret_triangle(1, 2, 3), 'Scalene')
        self.assertEqual(interpret_triangle(1, 2, 2), 'Isosceles')
        self.assertEqual(interpret_triangle(1, 1, 1), 'Equilateral')
        self.assertEqual(interpret_triangle(1, -1, 1), 'Error')
        self.assertEqual(interpret_triangle(1, 0, 1), 'Error')


if __name__ == '__main__':
    unittest.main()
